<%--
  Created by IntelliJ IDEA.
  User: Li
  Date: 2017/11/15
  Time: 21:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="layout/head.jsp" %>
<div class="module">
    <div class="module-head">
        <h3>${ msg.value}</h3>
    </div>
    <div class="module-body">
        <c:if test="${ msg != null}">
            <div class="alert alert-${ msg.type}">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>${ msg.value}</strong>
            </div>
        </c:if>
    </div>
</div>


<%@include file="layout/foot.jsp" %>