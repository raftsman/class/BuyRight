<%--
  Created by IntelliJ IDEA.
  User: Li
  Date: 2017/11/15
  Time: 18:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="../layout/head.jsp" %>

<div class="module">
    <div class="module-head">
        <h3>
            分类列表</h3>
    </div>
    <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0"
               class="datatable-1 table table-bordered table-striped display"
               width="100%">
            <thead>
            <tr>
                <th>
                    序号
                </th>
                <th>
                    分类名称
                </th>
                <th>
                    操作
                </th>

            </tr>
            </thead>
            <tbody>

            <c:forEach var="c" items="${ list }" varStatus="vs">

                <tr class="odd gradeX">
                    <td>
                        ${vs.count }
                    </td>
                    <td>
                        ${c.cname }
                    </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/admin/category?method=delCategoryById&id=${c.cid}">删除</a>
                    </td>
                </tr>
            </c:forEach>

            </tbody>

        </table>
    </div>
</div>
<!--/.module-->

<%@include file="../layout/foot.jsp" %>
