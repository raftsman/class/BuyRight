<%--
  Created by IntelliJ IDEA.
  User: Li
  Date: 2017/11/15
  Time: 18:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="../layout/head.jsp" %>

<div class="module">
    <div class="module-head">
        <h3>添加分类</h3>
    </div>
    <div class="module-body">

        <c:if test="${ msg != null}">
            <div class="alert alert-${ msg.type}">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>${ msg.value}</strong>
            </div>
        </c:if>


        <br/>
        <form class="form-horizontal row-fluid" action="${pageContext.request.contextPath}/admin/category?method=addCategory" method="post">
            <div class="control-group">
                <label class="control-label" for="basicinput">分类名称</label>
                <div class="controls">
                    <input type="text" id="basicinput" placeholder="分类名称" class="span8" name="cname" required>
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-info">添加</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!--/.module-->

<%@include file="../layout/foot.jsp" %>