<%--
  Created by IntelliJ IDEA.
  User: Li
  Date: 2017/11/15
  Time: 20:01
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="../layout/head.jsp" %>

<div class="module">
    <div class="module-head">
        <h3> 订单列表</h3>
    </div>
    <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0"
               class="datatable-1 table table-bordered table-striped display"
               width="100%">
            <thead>
            <tr>
                <th> 序号</th>
                <th> 订单编号</th>
                <th> 总金额</th>
                <th> 状态</th>
                <%--<th> 操作</th>--%>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="c" items="${ list }" varStatus="vs">
                <tr class="odd gradeX">
                    <td> ${vs.count } </td>
                    <td> ${c.oid } </td>
                    <td> ￥${c.total } </td>
                    <td>
                        <c:if test="${c.state == 0 }">未付款</c:if>
                        <c:if test="${c.state == 1 }">待发货 </c:if>
                        <c:if test="${c.state == 2 }">待收货</c:if>
                        <c:if test="${c.state == 3 }">已完成</c:if>
                    </td>
                    <%--<td> 编辑 删除</td>--%>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </div>
</div>
<!--/.module-->

<%@include file="../layout/foot.jsp" %>
