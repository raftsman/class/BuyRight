<%--
  Created by IntelliJ IDEA.
  User: Li
  Date: 2017/11/15
  Time: 16:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="layout/head.jsp" %>


<div class="btn-box-row row-fluid">
    <a href="${pageContext.request.contextPath}/admin/product?method=viewProducts" class="btn-box big span4"><i
            class=" icon-random"></i><b>${productnum}</b>
        <p class="text-muted">总商品数</p>
    </a><a href="${pageContext.request.contextPath}/admin/user?method=viewUsers" class="btn-box big span4"><i
        class="icon-user"></i><b>${ usernum }</b>
    <p class="text-muted">
        总用户</p></a>
    <a href="${pageContext.request.contextPath}/admin/order?method=viewOrders" class="btn-box big span4"><i
            class="icon-money"></i><b>${ ordernum }元</b>
        <p class="text-muted">
            总交易额</p>
    </a>
</div>

<div class="module">
    <div class="module-head">
        <h3>公告栏</h3>
    </div>
    <div class="module-body">
        <h1>尊敬的管理员<span class="text-info">${user.name}</span></h1>
        <h2>欢迎来到北半球第二好用的二手商品交易网站的后台管理系统！</h2>
    </div>
</div>

<%@include file="layout/foot.jsp" %>