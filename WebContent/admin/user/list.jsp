<%--
  Created by IntelliJ IDEA.
  User: Li
  Date: 2017/11/15
  Time: 19:13
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="../layout/head.jsp" %>

<div class="module">
    <div class="module-head">
        <h3>
            用户列表</h3>
    </div>
    <c:if test="${ msg != null}">
        <div class="alert alert-${ msg.type}">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>${ msg.value}</strong>
        </div>
    </c:if>
    <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0"
               class="datatable-1 table table-bordered table-striped display"
               width="100%">
            <thead>
            <tr>
                <th> 序号</th>
                <th> 用户名</th>
                <th> 性别</th>
                <th> email</th>
                <th> 电话</th>
                <th> 角色</th>
                <th> 操作</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach var="c" items="${ list }" varStatus="vs">

                <tr class="odd gradeX">
                    <td> ${vs.count } </td>
                    <td> ${c.username } </td>
                    <td>
                        <c:if test="${c.sex == 1}">
                            男
                        </c:if>
                        <c:if test="${c.sex == 0}">
                            女
                        </c:if>
                    </td>
                    <td> ${c.email } </td>
                    <td> ${c.telephone } </td>
                    <td>
                        <c:if test="${c.is_admin == 1}">
                            管理员
                        </c:if>
                        <c:if test="${c.is_admin == 0}">
                            普通用户
                        </c:if>
                    </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/admin/user?method=editUser&uid=${c.uid}">编辑</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<!--/.module-->

<%@include file="../layout/foot.jsp" %>
