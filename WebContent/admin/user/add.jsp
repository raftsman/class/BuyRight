<%--
  Created by IntelliJ IDEA.
  User: Li
  Date: 2017/11/15
  Time: 20:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="../layout/head.jsp" %>

<div class="module">
    <div class="module-head">
        <h3>添加用户</h3>
    </div>
    <div class="module-body">
        <c:if test="${ msg != null}">
            <div class="alert alert-${ msg.type}">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>${ msg.value}</strong>
            </div>
        </c:if>


        <br/>
        <form class="form-horizontal row-fluid" action="${pageContext.request.contextPath}/admin/user?method=addUser" method="post">
            <div class="control-group">
                <label class="control-label" for="basicinput">用户名</label>
                <div class="controls">
                    <input type="text" id="basicinput" placeholder="用户名" class="span8" name="username" required>
                    <span class="help-inline"></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="basicinput">密码</label>
                <div class="controls">
                    <input type="password" placeholder="密码" class="span8" name="password" required>
                    <span class="help-inline"></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="basicinput">确认密码</label>
                <div class="controls">
                    <input type="password" placeholder="确认密码" class="span8" name="confirmpwd" required>
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="basicinput">Email</label>
                <div class="controls">
                    <input type="email" placeholder="email" class="span8" name="email" required>
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="basicinput">姓名</label>
                <div class="controls">
                    <input type="text" placeholder="姓名" class="span8" name="name" required>
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="basicinput">性别</label>
                <div class="controls">
                    <select tabindex="1" data-placeholder="Select here.." class="span8" name="sex">
                        <option value="1">男</option>
                        <option value="0">女</option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">出生日期</label>
                <div class="controls">
                    <input type="date" class="form-control" name="birthday">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="basicinput">电话号码</label>
                <div class="controls">
                    <input type="number" placeholder="电话号码" class="span8" name="telephone" required>
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="basicinput">角色</label>
                <div class="controls">
                    <select tabindex="1" data-placeholder="Select here.." class="span8" name="is_admin">
                        <option value="0">普通用户</option>
                        <option value="1">管理员</option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-info">添加用户</button>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="../layout/foot.jsp" %>
