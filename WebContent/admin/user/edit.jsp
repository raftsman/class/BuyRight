<%--
  Created by IntelliJ IDEA.
  User: Li
  Date: 2017/11/15
  Time: 21:02
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="../layout/head.jsp" %>

<div class="module">
    <div class="module-head">
        <h3>编辑用户</h3>
    </div>
    <div class="module-body">
        <c:if test="${ msg != null}">
            <div class="alert alert-${msg.type}">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>${ msg.value}</strong>
            </div>
        </c:if>
        <br/>
        <form class="form-horizontal row-fluid" action="${pageContext.request.contextPath}/admin/user?method=editUser&uid=${user.uid}"
              method="post">
            <div class="control-group">
                <label class="control-label" for="basicinput">用户id</label>
                <div class="controls">
                    <input type="text" placeholder="用户名" class="span8" name="uid" value="${user.uid}" required disabled>
                    <span class="help-inline"></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="basicinput">用户名</label>
                <div class="controls">
                    <input type="text" id="basicinput" placeholder="用户名" class="span8" name="username"
                           value="${user.username}" required disabled>
                    <span class="help-inline"></span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="basicinput">密码</label>
                <div class="controls">
                    <input type="password" placeholder="密码" class="span8" name="password" value="${user.password}"
                           required>
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="basicinput">Email</label>
                <div class="controls">
                    <input type="email" placeholder="email" class="span8" name="email" value="${user.email}" required>
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="basicinput">姓名</label>
                <div class="controls">
                    <input type="text" placeholder="姓名" class="span8" name="name" value="${user.name}" required>
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="basicinput">性别</label>
                <div class="controls">
                    <select tabindex="1" data-placeholder="Select here.." class="span8" name="sex">
                        <option value="1"
                                <c:if test="${ user.sex == 1}">selected</c:if> >男
                        </option>
                        <option value="0"
                                <c:if test="${ user.sex == 0}">selected</c:if> >女
                        </option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">出生日期</label>
                <div class="controls">
                    <input type="date" class="form-control" name="birthday" value="${user.birthday}">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="basicinput">电话号码</label>
                <div class="controls">
                    <input type="number" placeholder="电话号码" class="span8" name="telephone" value="${user.telephone}" required>
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="basicinput">角色</label>
                <div class="controls">
                    <select tabindex="1" data-placeholder="Select here.." class="span8" name="is_admin">
                        <option value="0" <c:if test="${ user.is_admin == 0}">selected</c:if> >普通用户</option>
                        <option value="1" <c:if test="${ user.is_admin == 1}">selected</c:if> >管理员</option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn btn-info">更新</button>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="../layout/foot.jsp" %>
