<%--
  Created by IntelliJ IDEA.
  User: Li
  Date: 2017/11/15
  Time: 16:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

</div>
<!--/.content-->
</div>
<!--/.span9-->
</div>
</div>
<!--/.container-->
</div>
<!--/.wrapper-->
<div class="footer">
    <div class="container">
        <b class="copyright">&copy; 2017 Buyright - 这真的是北半球第二好用的二手交易网站 </b>
    </div>
</div>
<script src="${pageContext.request.contextPath}/resource/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/scripts/jquery-ui-1.10.1.custom.min.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/scripts/flot/jquery.flot.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/scripts/flot/jquery.flot.resize.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/scripts/datatables/jquery.dataTables.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/scripts/common.js" type="text/javascript"></script>

</body>
