<%--
  Created by IntelliJ IDEA.
  User: Li
  Date: 2017/11/15
  Time: 16:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="span3">
                <div class="sidebar">
                    <ul class="widget widget-menu unstyled">
                        <li class="active"><a href="${pageContext.request.contextPath}/admin"><i
                                class="menu-icon icon-dashboard"></i>系统总览
                        </a></li>
                        <li><a class="collapsed" data-toggle="collapse" href="#categoryTogglePages"><i
                                class="menu-icon icon-cog"></i>
                            <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"></i>
                            分类管理</a>
                            <ul id="categoryTogglePages" class="collapse unstyled">
                                <li><a href="${pageContext.request.contextPath}/admin/category?method=viewCategories"><i
                                        class="icon-inbox"></i>分类列表</a></li>
                                <li><a href="${pageContext.request.contextPath}/admin/category/add.jsp"><i
                                        class="icon-inbox"></i>添加分类</a></li>
                            </ul>
                        </li>
                        <li><a class="collapsed" data-toggle="collapse" href="#userTogglePages"><i
                                class="menu-icon icon-cog"></i>
                            <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"></i>
                            用户管理</a>
                            <ul id="userTogglePages" class="collapse unstyled">
                                <li><a href="${pageContext.request.contextPath}/admin/user?method=viewUsers"><i
                                        class="icon-inbox"></i>用户列表</a></li>
                                <li><a href="${pageContext.request.contextPath}/admin/user/add.jsp"><i class="icon-inbox"></i>添加用户</a></li>
                            </ul>
                        </li>
                        <li><a class="collapsed" data-toggle="collapse" href="#productTogglePages"><i
                                class="menu-icon icon-cog"></i>
                            <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"></i>
                            商品管理</a>
                            <ul id="productTogglePages" class="collapse unstyled">
                                <li><a href="${pageContext.request.contextPath}/admin/product?method=viewProducts"><i
                                        class="icon-inbox"></i>商品发布记录</a></li>
                            </ul>
                        </li>

                        <li><a class="collapsed" data-toggle="collapse" href="#orderTogglePages"><i
                                class="menu-icon icon-cog"></i>
                            <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"></i>
                            订单管理</a>
                            <ul id="orderTogglePages" class="collapse unstyled">
                                <li><a href="${pageContext.request.contextPath}/admin/order?method=viewOrders"><i
                                        class="icon-inbox"></i>订单列表</a></li>
                            </ul>
                        </li>
                    </ul>
                    <!--/.widget-nav-->


                    <!--/.widget-nav-->
                    <ul class="widget widget-menu unstyled">
                        <li><a href="${pageContext.request.contextPath}/admin_auth?type=logout"><i
                                class="menu-icon icon-signout"></i>注销 </a></li>
                    </ul>
                </div>
                <!--/.sidebar-->
            </div>
            <!--/.span3-->
            <div class="span9">
                <div class="content">