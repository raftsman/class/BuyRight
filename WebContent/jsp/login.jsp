<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/jsp/layout/head.jsp" %>
<div class="row">
    <div class="col-xs-12 col-md-4 col-md-offset-4">
        <div class="page-header">
            <h2>登录</h2>
        </div>
        <c:if test="${msg!=null}">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                    ${msg}
            </div>
        </c:if>
        <form class="form-horizontal" action="${pageContext.request.contextPath }/user" method="post">
            <input type="hidden" name="method" value="login">

            <div class="form-group">
                <label>用户名</label>

                <input type="text" class="form-control" id="username" placeholder="请输入用户名" name="username" required>

            </div>
            <div class="form-group">
                <label>密码</label>

                <input type="password" class="form-control" id="inputPassword3" placeholder="请输入密码"
                       name="password" required>

            </div>

            <div class="form-group">
                <div>
                    <div class="checkbox">
                        <label></label>
                        <input type="checkbox" name="savename" value="ok"> 记住我
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div>
                    <button type="submit" class="btn btn-primary">登录</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    var val = "${cookie.saveName.value}";
    document.getElementById("username").value = decodeURI(val);
</script>

<%@include file="/jsp/layout/foot.jsp" %>