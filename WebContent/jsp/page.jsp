<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!--分页 -->

<c:if test="${pb.totalPage > 1}">
    <div class="col-xs-12 col-md-10 col-md-offset-1">
        <ul class="pagination">

            <!-- 判断是否是第一页 -->
            <c:if test="${pb.pageNumber == 1 }">
                <li class="disabled">
                    <a href="javascript:void(0)" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            </c:if>

            <!-- 不是第一页 -->
            <c:if test="${pb.pageNumber != 1 }">
                <li>
                    <a href="${pageContext.request.contextPath }/order?method=findMyOrdersByPage&pageNumber=${pb.pageNumber-1}&cid=${param.cid}"
                       aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            </c:if>

            <!-- 展示所有页码 -->
            <c:forEach begin="1" end="${pb.totalPage }" var="n">
                <!-- 判断是否是当前页 -->
                <c:if test="${pb.pageNumber == n }">
                    <li class="active"><a href="javascript:void(0)">${n }</a></li>
                </c:if>
                <c:if test="${pb.pageNumber != n }">
                    <li>
                        <a href="${pageContext.request.contextPath }/order?method=findMyOrdersByPage&cid=${param.cid}&pageNumber=${n}">${n }</a>
                    </li>
                </c:if>
            </c:forEach>

            <!-- 判断是否是最后一页 -->
            <c:if test="${pb.pageNumber == pb.totalPage }">
                <li class="disabled">
                    <a href="javascript:void(0)" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </c:if>
            <c:if test="${pb.pageNumber != pb.totalPage }">
                <li>
                    <a href="${pageContext.request.contextPath }/order?method=findMyOrdersByPage&cid=${param.cid}&pageNumber=${pb.pageNumber+1}"
                       aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </c:if>

        </ul>
    </div>
</c:if>
<!-- 分页结束 -->