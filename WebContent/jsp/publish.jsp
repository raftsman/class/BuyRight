<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/jsp/layout/head.jsp" %>
<div class="row">
    <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="page-header">
            <h2>发布商品</h2>
        </div>

        <form id="userAction_save_do" name="Form1" action="${pageContext.request.contextPath}/addProduct" method="post"
              enctype="multipart/form-data">

            <div class="form-group">
                <label>商品名称</label>
                <input type="text" class="form-control" name="pname" id="userAction_save_do_logonName" required/>
            </div>
            <%--<div class="form-group">--%>
                <%--<label>是否热门</label>--%>
                <%--<select name="is_hot" class="form-control">--%>
                    <%--<option value="1">是</option>--%>
                    <%--<option value="0">否</option>--%>
                <%--</select>--%>
            <%--</div>--%>
            <div class="form-group">
                <label>新品购入价格￥</label>
                <input type="number" name="market_price" id="userAction_save_do_logonName" class="form-control" required/>
            </div>
            <div class="form-group">
                <label>价格￥</label>
                <input type="number" name="shop_price" id="userAction_save_do_logonName" class="form-control" required/>
            </div>
            <div class="form-group">
                <label>图片</label>
                <input type="file" name="pimage" class=""/>
            </div>
            <div class="form-group">
                <label>分类</label>
                <select name="cid" id="categoryList" class="form-control"></select>
            </div>
            <div class="form-group">
                <label>商品描述</label>
                <textarea name="pdesc" rows="5" cols="30" class="form-control" required></textarea>
            </div>

            <button type="submit" class="btn btn-primary">发布</button>
            <button type="reset" class="btn btn-warning">重置</button>

        </form>
    </div>
</div>
<script type="text/javascript">
    function subForm() {
        //让指定的表单提交
        document.getElementById("form1").submit();
    }

    $(function () {
        //发送ajax 查询所有分类
        $.post("${pageContext.request.contextPath}/category", {"method": "findAll"}, function (obj) {
            //alert(obj);
            //遍历json列表,获取每一个分类,包装成li标签,插入到ul内部
            //$.each($(obj),function(){});
            $(obj).each(function () {
                $("#categoryList").append("<OPTION value=" + this.cid + ">" + this.cname + "</OPTION>");
            });
        }, "json");
    })
</script>
<%@include file="/jsp/layout/foot.jsp" %>


