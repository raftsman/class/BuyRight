<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/jsp/layout/head.jsp" %>
<div class="row">
    <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="page-header ">
            <h3>注册</h3>
        </div>
        <form class="form-horizontal" method="post" action="${pageContext.request.contextPath }/user">
            <input type="hidden" name="method" value="regist">
            <div class="form-group">
                <label for="username" class="col-sm-2 control-label">用户名</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="username" placeholder="请输入用户名" name="username" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">密码</label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" id="inputPassword3" placeholder="请输入密码" name="password"
                           required>
                </div>
            </div>
            <div class="form-group">
                <label for="confirmpwd" class="col-sm-2 control-label">确认密码</label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" id="confirmpwd" placeholder="请输入确认密码" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-6">
                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email" required>
                </div>
            </div>

            <div class="form-group">
                <label for="usercaption" class="col-sm-2 control-label">姓名</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="usercaption" placeholder="请输入姓名" name="name" required>
                </div>
            </div>
            <div class="form-group opt">
                <label for="inlineRadio1" class="col-sm-2 control-label">性别</label>
                <div class="col-sm-6">
                    <label class="radio-inline">
                        <input type="radio" name="sex" id="inlineRadio1" value="1"> 男
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="sex" id="inlineRadio2" value="0"> 女
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">出生日期</label>
                <div class="col-sm-6">
                    <input type="date" class="form-control" name="birthday">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">手机号码</label>
                <div class="col-sm-6">
                    <input type="number" class="form-control" id="inputTelephone" placeholder="手机号码" name="telephone" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="submit" class="btn btn-primary">注册</button>
                    <a name="submit" class="btn btn-success"
                       href="${pageContext.request.contextPath}/user?method=loginUI">已有账号？去登录</a>
                </div>
            </div>

        </form>
    </div>

    <div class="col-md-2"></div>

</div>

<%@include file="/jsp/layout/foot.jsp" %>




