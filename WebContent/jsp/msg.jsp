<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/jsp/layout/head.jsp" %>

<h3 class="text-center">${msg}</h3>

<%@include file="/jsp/layout/foot.jsp" %>
