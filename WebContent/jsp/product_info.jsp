<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/jsp/layout/head.jsp" %>

<div class="panel panel-default col-xs-12 col-md-10 col-md-offset-1 row">

    <div class="panel-body">

        <div class="col-md-6">
            <img style="opacity: 1;width:400px;height:350px;" class="img-rounded"
                 src="${pageContext.request.contextPath}/${bean.pimage}">
        </div>
        <div class="col-md-4">
            <div><strong>${bean.pname }</strong></div>
            <div style="border-bottom: 1px dotted #dddddd;width:350px;margin:10px 0 10px 0;">
                <div>发布者：<a href="${pageContext.request.contextPath}/publish_list?uid=${prouser.uid}">${prouser.name }</a></div>
            </div>

            <div style="margin:10px 0 10px 0;">二手价: <strong style="color:#ef0101;">￥：${bean.shop_price }</strong>
                全新价：
                <del>￥${bean.market_price }</del>
            </div>

            <div style="padding:10px;width:330px;margin:15px 0 10px 0;">
                <form action="${pageContext.request.contextPath }/cart" id="form1" method="get">
                    <input type="hidden" name="method" value="add2cart">
                    <input type="hidden" name="pid" value="${bean.pid }">
                    <div style="margin-top:20px;padding-left: 10px;">购买数量:
                        <input id="quantity" name="count" value="1" maxlength="4" size="10" type="number" class="form-control" required></div>
                </form>
                <br>
                <buttom class="btn btn-success" onclick="subForm()">加入购物车</buttom>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default col-xs-12 col-md-10 col-md-offset-1 row">

    <div class="panel-body">
        ${bean.pdesc }
    </div>
</div>


<script type="text/javascript">
    function subForm() {
        //让指定的表单提交
        document.getElementById("form1").submit();
    }
</script>

<%@include file="/jsp/layout/foot.jsp" %>
