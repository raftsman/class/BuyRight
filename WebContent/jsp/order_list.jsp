<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/jsp/layout/head.jsp" %>
<div class="container">
    <div class="row">

        <div class="col-xs-12 col-md-10 col-md-offset-1">
            <div class="page-header">
                <h2>订单记录</h2>
            </div>
            <c:forEach items="${pb.data }" var="o">
                <table class="table table-bordered">

                    <tbody>
                    <tr class="success">
                        <th colspan="2">订单编号:${o.oid } </th>
                        <th colspan="1">
                            <c:if test="${o.state == 0 }"><a
                                    href="${pageContext.request.contextPath }/order?method=getById&oid=${o.oid}">去付款</a></c:if>
                            <c:if test="${o.state == 1 }">已付款</c:if>
                            <c:if test="${o.state == 2 }">确认收货</c:if>
                            <c:if test="${o.state == 3 }">已完成</c:if>
                        </th>
                        <th colspan="2">金额:${o.total }元</th>
                    </tr>
                    <tr class="warning">
                        <th>图片</th>
                        <th>商品</th>
                        <th>价格</th>
                        <th>数量</th>
                        <th>小计</th>
                    </tr>
                    <c:forEach items="${o.items }" var="oi">
                        <tr class="active">
                            <td width="60" width="40%">
                                <input type="hidden" name="id" value="22">
                                <img src="${pageContext.request.contextPath}/${oi.product.pimage}" width="70"
                                     height="60">
                            </td>
                            <td width="30%">
                                <a href="${pageContext.request.contextPath }/product?method=getById&pid=${oi.product.pid}" target="_blank">${oi.product.pname}</a>
                            </td>
                            <td width="20%">
                                ￥${oi.product.shop_price}
                            </td>
                            <td width="10%">
                                    ${oi.count }
                            </td>
                            <td width="15%">
                                <span class="subtotal">￥${oi.subtotal}</span>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

            </c:forEach>

        </div>
    </div>
    <%@include file="/jsp/page.jsp" %>
</div>

<%@include file="/jsp/layout/foot.jsp" %>