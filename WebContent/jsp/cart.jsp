<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/jsp/layout/head.jsp" %>

<div class="container">
    <div class="row">
        <c:if test="${empty cart || empty cart.cartItems }">
            <h3 class="text-center">购物车里什么都没有！</h3>
        </c:if>
        <c:if test="${not empty cart.cartItems}">
        <div style="margin:0 auto; margin-top:10px;width:950px;">
            <div class="page-header">
                <h2>我的购物车</h2>
            </div>
            <table class="table table-bordered">
                <tbody>
                <tr class="warning">
                    <th>图片</th>
                    <th>商品</th>
                    <th>价格</th>
                    <th>数量</th>
                    <th>小计</th>
                    <th>操作</th>
                </tr>
                <c:forEach items="${cart.cartItems }" var="ci">
                    <tr class="active">
                        <td width="60" width="40%">
                            <input type="hidden" name="id" value="22">
                            <img src="${pageContext.request.contextPath}/${ci.product.pimage}" width="70" height="60">
                        </td>
                        <td width="30%">
                            <a target="_blank" href="${pageContext.request.contextPath }/product?method=getById&pid=${ci.product.pid}">${ci.product.pname}</a>
                        </td>
                        <td width="20%">
                            ￥${ci.product.shop_price}
                        </td>
                        <td width="10%">
                            ${ci.count }
                        </td>
                        <td width="15%">
                            <span class="subtotal">￥${ci.subtotal }</span>
                        </td>
                        <td>
                            <a href="javascript:void(0);" onclick="removeFromCart('${ci.product.pid}')" class="delete"><i class="glyphicon glyphicon-trash"></i>删除</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <div style="margin-right:130px;">
        <div style="text-align:right;">
            商品金额: <strong style="color:#ff6600;">￥${cart.total }元</strong>
        </div>
        <div style="text-align:right;margin-top:10px;margin-bottom:10px;">
            <a href="${pageContext.request.contextPath }/cart?method=clear" id="clear" class="btn btn-danger">清空购物车</a>
            <a href="${pageContext.request.contextPath }/order?method=save" class="btn btn-success">提交订单</a>
        </div>
    </div>
    </c:if>
</div>

<script type="text/javascript">
    function removeFromCart(pid) {
        if (confirm("确定要删除该商品吗?")) {
            location.href = "${pageContext.request.contextPath}/cart?method=remove&pid=" + pid;
        }
    }
</script>
<%@include file="/jsp/layout/foot.jsp" %>