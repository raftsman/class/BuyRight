<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="/jsp/layout/head.jsp" %>

<div class="jumbotron">
    <h1>BuyRight
        <small>买/卖你所想</small>
    </h1>
    <p>这可能是北半球第二好的二手商品交易网站</p>
    <p><a class="btn btn-primary btn-lg" href="#new" role="button">开始购物！</a></p>
</div>

<div class="container-fluid">
    <div class="col-md-12" id="new">
        <h2>最新商品</h2>
    </div>

    <div class="col-md-12">
        <div class="col-md-6" style="text-align:center;height:200px;padding:0px;">
            <img src="${pageContext.request.contextPath}/resource/images/qiangou.jpg" width="516px" height="200px"
                 style="display: inline-block;">
        </div>

        <c:forEach items="${nList }" var="p">
            <div class="col-md-2" style="text-align:center;height:200px;padding:10px 0px;">
                <a href="${pageContext.request.contextPath }/product?method=getById&pid=${p.pid}">
                    <img src="${pageContext.request.contextPath}/${p.pimage}" width="130" height="130"
                         style="display: inline-block;">
                </a>
                <p><a href="${pageContext.request.contextPath }/product?method=getById&pid=${p.pid}"
                      style='color:#666'>${fn:substring(p.pname,0,10) }..</a></p>
                <p class="text-primary">&yen;${p.shop_price }</p>
            </div>
        </c:forEach></div>
</div>


<%@include file="/jsp/layout/foot.jsp" %>
