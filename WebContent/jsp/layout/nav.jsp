<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!--菜单栏-->


<%--<div class="container-fluid">--%>
    <%--<div class="col-md-9">--%>
        <%--<span><h1>BuyRight--%>
            <%--<small> 可能是北半球第二好用的二手商品交易网站</small>--%>
        <%--</h1></span>--%>
    <%--</div>--%>
<%--</div>--%>
<!--
导航条
-->
<div class="container-fluid">
    <nav class="navbar navbar-default navbar-static-top navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${pageContext.request.contextPath }">BuyRight</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" id="c_ul">
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <c:if test="${empty user }">
                        <li><a href="${pageContext.request.contextPath }/user?method=loginUI">登录</a></li>
                        <li><a href="${pageContext.request.contextPath }/user?method=registUI">注册</a></li>
                    </c:if>
                    <c:if test="${not empty user }">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">${user.name }<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="${pageContext.request.contextPath }/order?method=findMyOrdersByPage&pageNumber=1">我的订单</a>
                                </li>
                                <li><a href="${pageContext.request.contextPath}/jsp/publish.jsp">发布商品</a></li>
                                <li><a href="${pageContext.request.contextPath}/publish_list?uid=${user.uid}">查看我发布商品</a></li>

                                <li><a href="${pageContext.request.contextPath }/jsp/cart.jsp">购物车</a></li>
                                <li><a href="${pageContext.request.contextPath }/user?method=logout">退出</a></li>
                            </ul>
                        </li>
                    </c:if>


                </ul>
            </div>
        </div>

    </nav>
</div>

<script type="text/javascript">
    $(function () {
        //发送ajax 查询所有分类
        $.post("${pageContext.request.contextPath}/category", {"method": "findAll"}, function (obj) {
            //alert(obj);
            //遍历json列表,获取每一个分类,包装成li标签,插入到ul内部
            //$.each($(obj),function(){});
            $(obj).each(function () {
                //alert(this.cname);
                $("#c_ul").append("<li><a href='${pageContext.request.contextPath}/product?method=findByPage&pageNumber=1&cid=" + this.cid + "'>" + this.cname + "</a></li>");
            });
        }, "json");
    })
</script>
<div class="container" style="padding-top: 65px;font-family:'Microsoft YaHei',serif;">
