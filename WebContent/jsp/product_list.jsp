<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="/jsp/layout/head.jsp" %>


<div class="row" style="width:1210px;margin:0 auto;">
    <c:choose>
        <c:when test="${pb.totalRecord == 0}">
            <h2 class="text-center">该分类下没有商品，您可以先看下其他分类！</h2>
        </c:when>
        <c:otherwise>
            <c:forEach items="${pb.data }" var="p">
                <div class="col-md-2">
                    <a href="${pageContext.request.contextPath }/product?method=getById&pid=${p.pid}">
                        <img src="${pageContext.request.contextPath}/${p.pimage}" width="170" height="170"
                             style="display: inline-block;">
                    </a>
                    <p class="text-success">
                        <a href="${pageContext.request.contextPath }/product?method=getById&pid=${p.pid}">${fn:substring(p.pname,0,10) }..</a>
                    </p>
                    <p class="text-primary">二手价：&yen;${p.shop_price }</p>
                </div>
            </c:forEach>
        </c:otherwise>
    </c:choose>
</div>

<!--分页 -->
<c:if test="${pb.totalPage > 1 }">
    <ul class="pagination">
        <!-- 判断是否是第一页 -->
        <c:if test="${pb.pageNumber == 1 }">
            <li class="disabled">
                <a href="javascript:void(0)" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
        </c:if>
        <!-- 不是第一页 -->
        <c:if test="${pb.pageNumber != 1 }">
            <li>
                <a href="${pageContext.request.contextPath }/product?method=findByPage&pageNumber=${pb.pageNumber-1}&cid=${param.cid}"
                   aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
        </c:if>

        <!-- 展示所有页码 -->
        <c:forEach begin="1" end="${ pb.totalPage }" var="n">
            <!-- 判断是否是当前页 -->
            <c:if test="${pb.pageNumber == n }">
                <li class="active"><a href="javascript:void(0)">${n }</a></li>
            </c:if>
            <c:if test="${pb.pageNumber != n }">
                <li>
                    <a href="${pageContext.request.contextPath }/product?method=findByPage&cid=${param.cid}&pageNumber=${n}">${n }</a>
                </li>
            </c:if>
        </c:forEach>
        <!-- 判断是否是最后一页 -->
        <c:if test="${pb.pageNumber == pb.totalPage }">
            <li class="disabled">
                <a href="javascript:void(0)" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </c:if>
        <c:if test="${pb.pageNumber != pb.totalPage }">
            <li>
                <a href="${pageContext.request.contextPath }/product?method=findByPage&cid=${param.cid}&pageNumber=${pb.pageNumber+1}"
                   aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </c:if>
    </ul>
</c:if>
<!-- 分页结束 -->


<%@include file="/jsp/layout/foot.jsp" %>