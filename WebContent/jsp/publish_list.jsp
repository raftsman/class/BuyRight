<%--
  Created by IntelliJ IDEA.
  User: Li
  Date: 2017/11/14
  Time: 16:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/jsp/layout/head.jsp" %>


<div class="container">
    <div class="row">
        <c:choose>
            <c:when test="${pb == null}">
                <h2 class="text-center">该分类下没有商品，您可以先看下其他分类！</h2>
            </c:when>
            <c:otherwise>
                <div class="col-xs-12 col-md-10 col-md-offset-1">
                    <div class="page-header">
                        <h2>${user.name}发布的商品</h2>
                    </div>

                        <table class="table table-bordered">

                            <tbody>
                            <tr class="success">
                                <th colspan="5">商品编号:${p.pid} </th>
                            </tr>
                            <tr class="warning">
                                <th>图片</th>
                                <th>商品</th>
                                <th>新品价格</th>
                                <th>二手价格</th>
                                <th>描述</th>
                            </tr>
                            <c:forEach items="${pb}" var="p">
                            <tr class="active">
                                <td width="60" width="40%">
                                    <input type="hidden" name="id" value="22">
                                    <img src="${pageContext.request.contextPath}/${p.pimage}" width="70"
                                         height="60">
                                </td>
                                <td width="20%">
                                    <a href="${pageContext.request.contextPath }/product?method=getById&pid=${p.pid}"
                                       target="_blank">${p.pname}</a>
                                </td>
                                <td width="10%">
                                    ￥${p.market_price}
                                </td>
                                <td width="10%">
                                    ￥${p.shop_price}

                                </td>
                                <td width="35%">
                                    ${p.pdesc}
                                </td>
                            </tr>
                            </c:forEach>

                            </tbody>
                        </table>

                </div>
            </c:otherwise>
        </c:choose>

    </div>
</div>

<%@include file="/jsp/layout/foot.jsp" %>
