package com.buyright.web.filter;

import com.buyright.domain.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "AdminPrivilegeFilter", urlPatterns = "/admin/*")
public class AdminPrivilegeFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request =(HttpServletRequest) req;
        HttpServletResponse response =(HttpServletResponse) resp;

        //从session中获取用户
        User user = (User) request.getSession().getAttribute("admin");

        if(user == null){
            //未登录
            request.getRequestDispatcher("/admin/login.jsp").forward(request, response);
            return;
        }
        //3.放行

        chain.doFilter(request, response);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
