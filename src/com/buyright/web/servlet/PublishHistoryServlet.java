package com.buyright.web.servlet;

import com.buyright.dao.UserDao;
import com.buyright.dao.impl.UserDaoImpl;
import com.buyright.domain.Product;
import com.buyright.domain.User;
import com.buyright.service.ProductService;
import com.buyright.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "PublishHistoryServlet")
public class PublishHistoryServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User user = (User) request.getSession().getAttribute("user");
        if (user == null) {
            request.setAttribute("msg", "请先登录");
            request.getRequestDispatcher("/jsp/msg.jsp").forward(request,response);
        }else{
            try {

                String uid = request.getParameter("uid");
                if (uid==null)uid=user.getUid();

                UserDao u = new UserDaoImpl();
                User us = u.findUserById(uid);

                ProductService p =new ProductServiceImpl();
                List<Product> pb = p.findProductByUid(uid);
                request.setAttribute("user", us);
                request.setAttribute("pb", pb);
                request.getRequestDispatcher("/jsp/publish_list.jsp").forward(request,response);


            } catch (Exception e) {
                request.setAttribute("msg", "查询历史失败");
                request.getRequestDispatcher("/jsp/msg.jsp").forward(request,response);
            }

        }


    }

}
