package com.buyright.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.buyright.domain.Product;
import com.buyright.service.ProductService;
import com.buyright.service.impl.ProductServiceImpl;
import com.buyright.web.servlet.base.BaseServlet;

/**
	首页模块
 */
public class IndexServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public String index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			//1.调用productservice查询最新商品
			ProductService ps = new ProductServiceImpl();
			List<Product> newList=ps.findNew();

			request.setAttribute("nList", newList);
		} catch (Exception e) {
		}
		
		return "/jsp/index.jsp";
	}
}
