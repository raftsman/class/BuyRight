package com.buyright.web.servlet;

import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.buyright.domain.User;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import com.buyright.constant.Constant;
import com.buyright.domain.Category;
import com.buyright.domain.Product;
import com.buyright.service.ProductService;
import com.buyright.utils.BeanFactory;
import com.buyright.utils.UUIDUtils;
import com.buyright.utils.UploadUtils;

/**
 * 保存商品
 */
public class AddProductServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {

			//0.使用fileuload保存图片和将商品的信息放入map中
			//0.1 创建map 存放商品的信息
			Map<String,Object> map=new HashMap<>();
			
			//0.2 创建磁盘文件项工厂 (设置临时文件的大小和位置)
			DiskFileItemFactory factory = new DiskFileItemFactory();
			
			//0.3 创建核心上传对象
			ServletFileUpload upload = new ServletFileUpload(factory);
			
			//0.4 解析request
			List<FileItem> list = upload.parseRequest(request);
			
			//0.5遍历list 获取每一个文件项
			for (FileItem fi : list) {
				//0.6获取name属性值
				String key = fi.getFieldName();
				
				//0.7判断是否是普通的上传组件
				if(fi.isFormField()){
					//普通
					map.put(key, fi.getString("utf-8"));
				}else{
					//文件
					//a.获取文件的名称  1.jpg
					String name = fi.getName();
					
					//b.获取文件真实名称 1.jpg
					String realName = UploadUtils.getRealName(name);
					
					//c.获取文件的随机名称  12312312434234.jpg
					String uuidName = UploadUtils.getUUIDName(realName);
					
					//d.获取随机目录 /a/3
					String dir = UploadUtils.getDir();
					
					//e.获取文件内容(输入流)
					InputStream is = fi.getInputStream();
					
					//f.创建输出流
					//获取products目录的真实路径
					String productPath = getServletContext().getRealPath("/products");
					
					//创建随机目录
					File dirFile = new File(productPath,dir);
					if(!dirFile.exists()){
						dirFile.mkdirs();
					}
					
					// d:/tomcat/webapps/store/prouduct/a/3/12312312434234.jpg
					FileOutputStream os = new FileOutputStream(new File(dirFile,uuidName));
					
					//g.对拷流
					IOUtils.copy(is, os);
					
					//h.释放资源
					os.close();
					is.close();
					
					//i.删除临时文件
					fi.delete();
					
					//j.将商品的路径放入map中   prouduct/a/3/12312312434234.jpg
					map.put(key, "products"+dir+"/"+uuidName);
				}
			}
			
			//1.封装product对象
			Product p = new Product();
			//1.1.手动设置 pid
            String pid = UUIDUtils.getId();
			map.put("pid", pid);
			
			//1.2.手动设置 pdate
			map.put("pdate", new Date());

            //从session中获取用户
            User user = (User) request.getSession().getAttribute("user");
			p.setUid(user.getUid());
//            map.put("uid", user.getUid());


			//1.3.手动设置 pflag  上架
			map.put("pflag", Constant.PRODUCT_IS_UP);
			
			//1.4.使用beanutils封装
			BeanUtils.populate(p, map);
			
			//1.5.手动设置 category
			Category c = new Category();
			c.setCid((String)map.get("cid"));
			p.setCategory(c);

			//2.调用service 完成保存
			ProductService ps = (ProductService) BeanFactory.getBean("ProductService");
			ps.save(p);



            //3.重定向

            String path = request.getContextPath();
            String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
			request.setAttribute("msg", "发布成功！<a href=\""+ basePath +"product?method=getById&pid="+pid+"\">现在就去看看</a>");
            request.getRequestDispatcher("/jsp/msg.jsp").forward(request,response);
//			response.sendRedirect(request.getContextPath()+"/jsp/msg.jsp");
//			response.getWriter().println("发布成功！");
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("保存商品失败");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
