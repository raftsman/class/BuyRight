package com.buyright.web.servlet.admin;

import com.buyright.constant.Constant;
import com.buyright.domain.User;
import com.buyright.service.UserService;
import com.buyright.service.impl.UserServiceImpl;
import com.buyright.utils.BeanFactory;
import com.buyright.utils.UUIDUtils;
import com.buyright.web.servlet.base.BaseServlet;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "AdminUserServlet", urlPatterns = "/admin/user")
public class AdminUserServlet extends BaseServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public String viewUsers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            UserService users = (UserService) BeanFactory.getBean("UserService");
            List<User> list=users.findList();

            request.setAttribute("list", list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }

        return "/admin/user/list.jsp";
    }

    public String addUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map message = new HashMap();
        try {
            //1.封装对象
            User user = new User();
            BeanUtils.populate(user, request.getParameterMap());

            //1.1手动封装uid
            user.setUid(UUIDUtils.getId());

            //2.调用service完成注册
            UserService us=new UserServiceImpl();
            us.regist(user);

            //3.页面转发 提示信息
            message.put("type", "success");
            message.put("value", "用户添加成功!");
            request.setAttribute("msg", message);
            return "/admin/user/add.jsp";

        }catch (Exception e) {
            e.printStackTrace();
            //转发到 msg.jsp
            message.put("type", "error");
            message.put("value", "用户添加失败!");
            request.setAttribute("msg", message);
            return "/admin/user/add.jsp";
        }

    }

    public String editUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map message = new HashMap();
        String httpMethod = request.getMethod();
        String uid = request.getParameter("uid");

        if(uid==null) {
            User user = (User) request.getSession().getAttribute("admin");
            uid=user.getUid();
        }

        if (httpMethod.equals("GET")) {
            UserService us = new UserServiceImpl();
            User user = null;
            try {
                 user = us.findUserById(uid);
            } catch (Exception e) {
                e.printStackTrace();
                return "/admin/index.jsp";
            }

            request.setAttribute("user", user);
            return "/admin/user/edit.jsp";
        }else if(httpMethod.equals("POST")) {
            try {
                //1.封装对象
                User user = new User();
                BeanUtils.populate(user, request.getParameterMap());

                //2.调用service完成注册
                UserService us=new UserServiceImpl();
                us.update(user);

                //3.页面转发 提示信息
                message.put("type", "success");
                message.put("value", "更新用户信息成功!");
                request.setAttribute("msg", message);
                return "/admin/message.jsp";

            }catch (Exception e) {
                e.printStackTrace();
                //转发到 msg.jsp
                message.put("type", "error");
                message.put("value", "更新用户信息失败!");
                request.setAttribute("msg", message);
                return "/admin/message.jsp";
            }


        }
        return "/admin/user/edit.jsp";

    }
}
