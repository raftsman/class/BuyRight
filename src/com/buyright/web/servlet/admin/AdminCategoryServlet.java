package com.buyright.web.servlet.admin;

import com.buyright.domain.Category;
import com.buyright.service.CategoryService;
import com.buyright.utils.BeanFactory;
import com.buyright.utils.UUIDUtils;
import com.buyright.web.servlet.base.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "AdminCategoryServlet", urlPatterns = "/admin/category")
public class AdminCategoryServlet extends BaseServlet {
    public String viewCategories(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            CategoryService cs = (CategoryService) BeanFactory.getBean("CategoryService");
            List<Category> list=cs.findList();
            request.setAttribute("list", list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return "/admin/category/list.jsp";
    }

    public String addCategory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map message = new HashMap();
        try {
            Category c = new Category();
            c.setCid(UUIDUtils.getId());
            c.setCname(request.getParameter("cname"));

            CategoryService cs = (CategoryService) BeanFactory.getBean("CategoryService");
            cs.save(c);

            message.put("type", "success");
            message.put("value", "添加成功！");
            request.setAttribute("msg", message);
            return "/admin/category/add.jsp";
        } catch (Exception e) {
            e.printStackTrace();
            message.put("type", "error");
            message.put("value", "添加失败！");
            request.setAttribute("msg", message);
            return "/admin/category/add.jsp";
        }
    }

    public String delCategoryById(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map message = new HashMap();
        String cid = request.getParameter("id");
        if (cid!=null){
            CategoryService cs = (CategoryService) BeanFactory.getBean("CategoryService");
            try {
                boolean res = cs.delById(cid);

                if (res) {
                    message.put("type", "success");
                    message.put("value", "删除成功！");
                }else {
                    message.put("type", "error");
                    message.put("value", "删除失败！");
                }
            } catch (Exception e) {
                e.printStackTrace();
                message.put("type", "error");
                message.put("value", "删除失败！");
            }
        } else {
            message.put("type", "error");
            message.put("value", "删除失败！");
        }

        request.setAttribute("msg", message);
        return "/admin/message.jsp";
    }
}
