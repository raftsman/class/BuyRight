package com.buyright.web.servlet.admin;

import com.buyright.domain.Order;
import com.buyright.domain.Product;
import com.buyright.domain.User;
import com.buyright.service.OrderService;
import com.buyright.service.ProductService;
import com.buyright.service.UserService;
import com.buyright.service.impl.OrderServiceImpl;
import com.buyright.service.impl.ProductServiceImpl;
import com.buyright.service.impl.UserServiceImpl;
import com.sun.org.apache.xpath.internal.operations.Or;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "AdminIndexServlet", urlPatterns = "/admin")
public class AdminIndexServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserService us = new UserServiceImpl();
        ProductService ps = new ProductServiceImpl();
        OrderService os = new OrderServiceImpl();
        User user = (User) request.getSession().getAttribute("admin");

        try {
            List<User> users = us.findList();
            List<Product> products = ps.findAll();
            List<Order> orders = os.findAllByState("*");

            int count = 0;
            for (Order o : orders) {
                count += o.getTotal();
            }
            request.setAttribute("ordernum",count);
            request.setAttribute("usernum",users.size());
            request.setAttribute("productnum",products.size());
            request.setAttribute("user", user);
            request.getRequestDispatcher("/admin/index.jsp").forward(request, response);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
