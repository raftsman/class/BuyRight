package com.buyright.web.servlet.admin;

import com.buyright.constant.Constant;
import com.buyright.domain.User;
import com.buyright.service.UserService;
import com.buyright.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet(name = "AuthServlet", urlPatterns = "/admin_auth")
public class AuthServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //1.获取用户名和密码
            String username = request.getParameter("username");
            String password	= request.getParameter("password");

            //2.调用service完成登录 返回值:user
            UserService us = new UserServiceImpl();
            User user=us.login(username,password);

            //3.判断user 根据结果生成提示
            if(user == null){
                //用户名和密码不匹配
                request.setAttribute("msg", "用户名和密码不匹配");;
                request.getRequestDispatcher("/admin/login.jsp").forward(request, response);
                return;
            }

            //若用户不为空,继续判断是否有权限
            if(Constant.USER_IS_ADMIN != user.getIs_admin()){
                request.setAttribute("msg", "该用户没有权限");;
                request.getRequestDispatcher("/admin/login.jsp").forward(request, response);
                return;
            }

            //登录成功 保存用户登录状态
            request.getSession().setAttribute("admin", user);

            //跳转到 index.jsp
            response.sendRedirect(request.getContextPath()+"/admin");

        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("msg", "登录失败");;
            request.getRequestDispatcher("/admin/login.jsp").forward(request, response);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String type = request.getParameter("type");
        if(type.equals("logout")){
            request.getSession().setAttribute("admin", null);
            response.sendRedirect(request.getContextPath());
        }

    }
}
