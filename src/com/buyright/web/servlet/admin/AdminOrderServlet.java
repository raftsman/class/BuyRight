package com.buyright.web.servlet.admin;

import com.buyright.constant.Constant;
import com.buyright.domain.Order;
import com.buyright.domain.OrderItem;
import com.buyright.service.OrderService;
import com.buyright.utils.BeanFactory;
import com.buyright.utils.JsonUtil;
import com.buyright.web.servlet.base.BaseServlet;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "AdminOrderServlet", urlPatterns = "/admin/order")
public class AdminOrderServlet extends BaseServlet {
    public String showDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //0.设置编码
            response.setContentType("text/html;charset=utf-8");

            //1.获取oid
            String oid = request.getParameter("oid");

            //2.调用service 获取订单
            OrderService os = (OrderService) BeanFactory.getBean("OrderService");
            Order order = os.getById(oid);

            //3.获取订单的订单项列表 转成json 写回浏览器
            if(order != null){
                List<OrderItem> list = order.getItems();
                if(list != null && list.size()>0){
                    //response.getWriter().println(JsonUtil.list2json(list));

                    JsonConfig config = JsonUtil.configJson(new String[]{"order","pdate","pdesc","itemid"});
                    response.getWriter().println(JSONArray.fromObject(list, config));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String viewOrders(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            OrderService os = (OrderService) BeanFactory.getBean("OrderService");
            List<Order> list = os.findAllByState("*");

            //3.将list放入request域中,请求转发
            request.setAttribute("list", list);

        } catch (Exception e) {
        }
        return "/admin/order/list.jsp";
    }
}
