package com.buyright.web.servlet.admin;

import com.buyright.domain.Product;
import com.buyright.service.ProductService;
import com.buyright.service.impl.ProductServiceImpl;
import com.buyright.utils.BeanFactory;
import com.buyright.web.servlet.base.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "AdminProductServlet", urlPatterns = "/admin/product")
public class AdminProductServlet extends BaseServlet {

    public String viewProducts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //1.调用service 查询以上架商品
            ProductService ps = (ProductService) BeanFactory.getBean("ProductService");
            List<Product> list = ps.findAll();

            //2.将返回值放入request中,请求转发
            request.setAttribute("list", list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return "/admin/product/list.jsp";
    }

    public String delProductById(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pid = request.getParameter("id");
        Map message = new HashMap();

        try {
            ProductService ps = new ProductServiceImpl();

            boolean res = ps.delById(pid);

            if (res) {

                message.put("type", "success");
                message.put("value", "删除成功！");
                request.setAttribute("msg", message);

                return "/admin/message.jsp";

            } else {
                message.put("type", "error");
                message.put("value", "删除失败！");
                request.setAttribute("msg", message);

                return "/admin/message.jsp";
            }


        } catch (Exception e) {
            message.put("type", "error");
            message.put("value", "删除失败！");
            request.setAttribute("msg", message);

            return "/admin/message.jsp";

        }
    }
}
