package com.buyright.dao;

import com.buyright.domain.Category;
import com.buyright.domain.User;

import java.util.List;

public interface UserDao {

	void save(User user) throws Exception;

	User getByCode(String code) throws Exception;

	void update(User user) throws Exception;

    User getByUsernameAndPwd(String username, String password) throws Exception;

	User findUserById(String uid) throws Exception;

	List<User> findAll() throws Exception;
}
