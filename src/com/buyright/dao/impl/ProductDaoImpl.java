package com.buyright.dao.impl;

import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.buyright.constant.Constant;
import com.buyright.dao.ProductDao;
import com.buyright.domain.PageBean;
import com.buyright.domain.Product;
import com.buyright.utils.DataSourceUtils;

public class ProductDaoImpl implements ProductDao {


    @Override
    /**
     * 查询最新
     */
    public List<Product> findNew() throws Exception {
        QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
        String sql = "select * from product where pflag = ? order by pdate desc limit 21";
        return qr.query(sql, new BeanListHandler<>(Product.class), Constant.PRODUCT_IS_UP);
    }

    @Override
    /**
     * 查询单个商品
     */
    public Product getById(String pid) throws Exception {
        QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
        String sql = "select * from product where pid = ? limit 1";
        return qr.query(sql, new BeanHandler<>(Product.class), pid);

    }

    @Override
    /**
     * 查询当前页数据
     */
    public List<Product> findByPage(PageBean<Product> pb, String cid) throws Exception {
        QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
        String sql = "select * from product where cid = ? and pflag = ? order by pdate desc limit ?,?";
        return qr.query(sql, new BeanListHandler<>(Product.class), cid, Constant.PRODUCT_IS_UP, pb.getStartIndex(), pb.getPageSize());
    }

    @Override
    /**
     * 获取总记录数
     */
    public int getTotalRecord(String cid) throws Exception {
        return ((Long) new QueryRunner(DataSourceUtils.getDataSource()).query("select count(*) from product where cid = ? and pflag = ?", new ScalarHandler(), cid, Constant.PRODUCT_IS_UP)).intValue();
    }

    @Override
    /**
     * 后台展示上架商品
     */
    public List<Product> findAll() throws Exception {
        QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
        String sql = "select * from product where pflag = ? order by pdate desc";
        return qr.query(sql, new BeanListHandler<>(Product.class), Constant.PRODUCT_IS_UP);
    }

    /**
     *查询用户发布数据
     * @param uid
     * @return
     * @throws Exception
     */
    @Override
    public List<Product> findProductByUid(String uid) throws Exception {
        QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
        String sql = "select * from product where uid=?";
        return qr.query(sql, new BeanListHandler<>(Product.class), uid);
    }

    @Override
    /**
     * 保存商品
     */
    public void save(Product p) throws Exception {
        QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());

        String sql = "insert into product values(?,?,?,?,?,?,?,?,?,?);";
        qr.update(sql, p.getPid(), p.getPname(), p.getMarket_price(),
                p.getShop_price(), p.getPimage(), p.getPdate(),
                p.getUid(), p.getPdesc(), p.getPflag(),
                p.getCategory().getCid());
    }


    @Override
    public boolean delById(String cid) throws Exception {
        QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
        String sql = "delete from product where pid = ?";
        int rtn = qr.update(sql, cid);

        return rtn == 1;
    }
}
