package com.buyright.dao;

import java.util.List;

import com.buyright.domain.PageBean;
import com.buyright.domain.Product;

public interface ProductDao {

	List<Product> findNew() throws Exception;

	Product getById(String pid) throws Exception;

	List<Product> findByPage(PageBean<Product> pb, String cid) throws Exception;

	int getTotalRecord(String cid) throws Exception;

	List<Product> findAll() throws Exception;

	void save(Product p) throws Exception;

	List<Product> findProductByUid(String uid) throws Exception;

	boolean delById(String cid) throws Exception;

}
