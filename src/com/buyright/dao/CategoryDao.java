package com.buyright.dao;

import java.util.List;

import com.buyright.domain.Category;

public interface CategoryDao {

	List<Category> findAll() throws Exception;

	void save(Category c) throws Exception;

	boolean delById(String id) throws Exception;

}
