package com.buyright.utils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RedirectUtils {
    public static void redirectWithMessage(String message, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("msg", "你好");
        request.getRequestDispatcher("/jsp/msg.jsp").forward(request,response);
    }
}
