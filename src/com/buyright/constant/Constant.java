package com.buyright.constant;

public interface Constant {
	/**
	 * 普通用户
	 */
	int USER_IS_NOT_ADMIN = 0;
	
	
	/**
	 * 管理员用户
	 */
	int USER_IS_ADMIN = 1;
	
	
	/**
	 * 记住用户名
	 */
	String SAVE_NAME ="ok";
	
	/**
	 * 商品未下架
	 */
	int PRODUCT_IS_UP = 0;
	
	/**
	 * 商品已下架
	 */
	int PRODUCT_IS_DOWN = 1;
	
	/**
	 * 订单状态 未付款
	 */
	int ORDER_WEIFUKUAN=0;
	
	/**
	 * 订单状态 已付款
	 */
	int ORDER_YIFUKUAN=1;
	
	/**
	 * 订单状态 已发货
	 */
	int ORDER_YIFAHUO=2;
	
	/**
	 * 订单状态 已完成
	 */
	int ORDER_YIWANCHENG=3;
}
