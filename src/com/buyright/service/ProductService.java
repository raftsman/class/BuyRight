package com.buyright.service;

import java.util.List;

import com.buyright.domain.PageBean;
import com.buyright.domain.Product;

public interface ProductService {

	List<Product> findNew() throws Exception;

	Product getById(String pid) throws Exception;

	PageBean<Product> findByPage(int pageNumber, int pageSize, String cid) throws Exception;

	List<Product> findAll() throws Exception;

    List<Product> findProductByUid(String uid) throws Exception;

    void save(Product p)throws Exception;

    boolean delById(String pid)throws Exception;


}
