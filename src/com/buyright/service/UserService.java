package com.buyright.service;

import com.buyright.domain.User;

import java.util.List;

public interface UserService {

	void regist(User user) throws Exception;

    void update(User user) throws Exception;

	User login(String username, String password) throws Exception;

	User findUserById(String uid) throws Exception;

    List<User> findList() throws Exception;

}
