package com.buyright.service.impl;

import java.util.List;

import com.buyright.dao.CategoryDao;
import com.buyright.domain.Category;
import com.buyright.service.CategoryService;
import com.buyright.utils.BeanFactory;
import com.buyright.utils.JsonUtil;

import redis.clients.jedis.Jedis;

public class CategoryServiceImpl implements CategoryService {

	@Override
	/**
	 * 后台展示所有分类
	 */
	public List<Category> findList() throws Exception {
		CategoryDao cd = (CategoryDao) BeanFactory.getBean("CategoryDao");
		return cd.findAll();
	}

	@Override
	/**
	 * 查询所有分类
	 */
	public String findAll() throws Exception {
		/*//1.调用dao 查询所有分类
		CategoryDao cd = new CategoryDaoImpl();
		List<Category> list = cd.findAll();*/
		
		List<Category> list=findList();
		
		//2.将list转换成json字符串
		if(list!=null && list.size()>0){
			return JsonUtil.list2json(list);
		}
		return null;
	}

	@Override
	/**
	 * 从mysql中获取所有的分类
	 */
	public String findAllFromRedis() throws Exception {

		Jedis j =null;
		String value=null;

		value = findAll();
		return value;
	}

	@Override
	/**
	 * 添加分类
	 */
	public void save(Category c) throws Exception {
		//1.调用dao 完成添加
		CategoryDao cd = (CategoryDao) BeanFactory.getBean("CategoryDao");
		cd.save(c);

	}

    @Override
    public boolean delById(String id) throws Exception {
        CategoryDao cd = (CategoryDao) BeanFactory.getBean("CategoryDao");

        return cd.delById(id);
    }
}
