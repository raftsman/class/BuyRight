package com.buyright.service.impl;

import com.buyright.constant.Constant;
import com.buyright.dao.UserDao;
import com.buyright.domain.User;
import com.buyright.service.UserService;
import com.buyright.utils.BeanFactory;

import java.util.List;

public class UserServiceImpl implements UserService {

	@Override
	/**
	 * 用户注册
	 */
	public void regist(User user) throws Exception {
		//1.调用dao完成注册
		UserDao ud=(UserDao) BeanFactory.getBean("UserDao");
		ud.save(user);

	}

    @Override
    public void update(User user) throws Exception {
        UserDao ud=(UserDao) BeanFactory.getBean("UserDao");
        ud.update(user);
    }

	@Override
	/**
	 * 用户登录
	 */
	public User login(String username, String password) throws Exception {
		UserDao ud=(UserDao) BeanFactory.getBean("UserDao");
		
		return ud.getByUsernameAndPwd(username,password);
	}

    @Override
    public User findUserById(String uid) throws Exception {
        UserDao ud=(UserDao) BeanFactory.getBean("UserDao");
        return ud.findUserById(uid);
    }

	@Override
	public List<User> findList() throws Exception {
        UserDao cd = (UserDao) BeanFactory.getBean("UserDao");
        return cd.findAll();
	}
}
